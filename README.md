[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)

Pouf
----------
**Une nouvelle version de Froxiss est désormais disponible. Il paraît qu'elle est moins bugguée et qu'elle a été refondue entièrement. Si ça vous tente, c'est [par ici](https://framagit.org/Erase/Froxiss-Reload/)**. Dans le cas contraire, je vous offre un gif de chat.
-
![alt text](http://perdstontemps.ca/wp-content/uploads/2013/08/24.gif)
-
-
-
-
-


Avant tout
----------
**Le projet est actuellement ~~en cours de développement~~ abandonné (pour l'instant ?). Il est distribué uniquement à titre expérimental et à des fins de tests.
Pour procéder à l'installation, [c'est par ici]**.


[Du blabla présentant la sortie du projet par ici](http://blog.green-effect.fr/article/froxiss-le-sonarrss-a-larrache)

Froxiss
--------

Froxiss est un agrégateur de flux RSS simple et auto hébergeable tels que [Leed] ou [Miniflux].

Simple d'installation et de paramétrage, il ne nécessite aucune base de données et se veut volontairement épuré en fonctionnalités. Un simple serveur web tel que Apache2 lui convient.

Froxiss est mono-utilisateur et ne requiert aucune compétence technique particulière.


 * Sans base de données
 * Script auto hébergeable
 * Gestion des flux RSS/ATOM
 * Regroupement de flux en catégories personnalisées/personnalisable
 * Gestion du multi-flux
 * Espace privé
 * Import/Export OPML
 * Flux RSS global
 * Interface sous forme de grille aux placements et dimensions personnalisables
 * Gestion de thèmes 
 * Surcharge de CSS personnalisé dans espace `Paramètres`
 * Responsive Design
 * Gestion WebApp Firefox
 * Repartage de lien via [Shaarli]


Prérequis
---------

 * Serveur Web (tel que Apache2)
 * A partir de PHP 5.4 avec les modules suivants : `cUrl`, `JSON`, `SimpleXML`, `DOMDocument` et `PCRE`


Installation
------------

Récupération des sources
```sh
$ git clone https://git.framasoft.org/Erase/Froxiss.git
```
ou en [téléchargeant l'archive zip]

Vérifier/assigner les droits en écriture au sein des répertoires :
 * `/data`
 * `/system`
 * `/system/config`

Dans le cas d'une installation dans un sous répertoire, éditer le contenu du fichier `/system/config/pathconfig.php`

A l'aide de votre navigateur, se rendre sur la page du projet et renseigner un nom d'utilisateur et un mot de passe. 


Informations secondaires
------------------------

Une partie des options de configuration est gérée directement dans l'espace `Paramètres`. Toutefois, d'autres options disponibles dans le fichier `/system/config/default.php` sont personnalisables en les dupliquant directement dans le fichier `/system/config/localconfig.php` puis en procédant aux modifications voulues.

Le répertoire `/data/` contient les différents paramètres utilisateurs (personnalisations des flux, des catégories, ...) et les favicons des différents sites. 
Le répertoire `/system/cache/` contient le contenu des flux RSS mis en cache par SimplePie.
Le répertoire `/system/logs/` contient d'éventuels logs PHP (selon paramétrage).


A suivre
--------
Evolutions à venir (en vrac) :
 * Optimisations CSS et PHP
 * Documentation et nettoyage PHP
 * Abonnement à des flux non RSS via [rss-bridge]
 * Ajout d'une vue mode "Lecture"
 * Gestion de mots clés et black word
 * Récapitulatif personnalisé par mail via cron
 * Ajout d'un mode "Aperçu"


Licence
-------

 En dehors des différentes licences spécifiques aux outils utilisés, le reste du code est distribué sous licence [Creative Commons BY-NC-SA 4.0]


Auteur
------
 * [Un simple développeur paysagiste] :) avec un peu de temps libre et aucune prétention - contact_at_green-effect.fr


Librairies annexes et outils utilisés
-------------------------------------

Pour faire fonctionner Froxiss, un ensemble d'outils ont été directement utilisés :
 * [jQuery] : JavaScript Library
 * [Sizzle] : JavaScript CSS selector engine
 * [Gridster] : jQuery plugin for drag-and-drop multi-column grid 
 * [Sortable] : JavaScript library for reorderable drag-and-drop lists
 * [ColorPicker] : jQuery plugin for color picker
 * [SimplePie] : PHP script Feed parser
 * [FaviconDownloader] : PHP script find and download favicon
 * [Lib_OPML] : Manage OPML with PHP
 * [Auto_restrict] : PHP script by [Bronco] (<3) for restrict access


Un grand merci donc aux différents créateurs et contributeurs ! Par ailleurs, de larges morceaux de code ou approches ont été empruntés au fabuleux CMS/CMF qu'est [Contao], distribué sous licence LGPL et mis au point par Léo Feyer. Je ne cesse d'apprécier constamment son approche, la dynamique qu'il insuffle et son enthousiasme débordant. 
Tout plein de remerciements en particulier à [Bronco] pour l'idée, le soutien, les tests et l'énorme support moral (T'es un chef o/)


L'icône de Froxiss provient de [Freepik] distribuée sur [Flaticon], sous licence [CC BY 3.0].

[//]: # 

   
   [téléchargeant l'archive zip]: <https://git.framasoft.org/Erase/Froxiss/repository/archive.zip>
   [Miniflux]: <https://miniflux.net/>
   [Leed]: <https://github.com/ldleman/Leed>
   [jQuery]: <https://jquery.com/>
   [Sizzle]: <https://github.com/jquery/sizzle/>
   [Gridster]: <https://github.com/ducksboard/gridster.js>
   [Sortable]: <https://github.com/RubaXa/Sortable>
   [ColorPicker]: <https://github.com/tkrotoff/jquery-simplecolorpicker>
   [SimplePie]: <http://simplepie.org/>
   [FaviconDownloader]: <https://github.com/vincepare/FaviconDownloader>
   [Lib_OPML]: <https://github.com/marienfressinaud/lib_opml>
   [Auto_restrict]: <https://github.com/broncowdd/auto_restrict>
   [Contao]: <https://contao.org/>
   [Bronco]: <http://warriordudimanche.net/>
   [Creative Commons BY-NC-SA 4.0]: <http://creativecommons.org/licenses/by-nc-sa/4.0/>
   [Flaticon]: <http://www.flaticon.com>
   [Freepik]: <http://www.freepik.com>
   [CC BY 3.0]: <http://creativecommons.org/licenses/by/3.0/>
   [rss-bridge]: <https://github.com/sebsauvage/rss-bridge>
   [Shaarli]: <https://github.com/sebsauvage/Shaarli>
   [Un simple développeur paysagiste]: <http://www.green-effect.fr>
   [c'est par ici]: <https://git.framasoft.org/Erase/Froxiss#installation>