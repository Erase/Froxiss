var gridster;
$(document).ready(function(){
    function contextMenu(el, options){
        var defaults = {
            menu: null,
            offsetX : 8,
            offsetY : 8,
            speed : 'fast'
        };

        var $el = $(el);
        var options = $.extend(defaults, options);
        var $menu_item = $(options.menu);

        $el.bind("contextmenu",function(e){
            return false;
        });

        $el.mousedown(function(e){
            var $this = $(this);
            var $cible = $this.next(options.menu);
            $menu_item.hide();

            if(e.button == "2"){  
                var left_pos = e.pageX+options.offsetX-$this.offset().left-$cible.width();
                var top_pos = e.pageY+options.offsetY-$this.offset().top;
            
                $cible.show(options.speed).css({
                    'display' : 'block',
                    'top'   : top_pos,
                    'left'  : left_pos
                });

                $cible.mouseleave(function(){
                   $cible.hide(options.speed);
                });
            }
        });

        $menu_item.find('.shaarlier a').click(function(){
            var $this = $(this);
            var $e = $this.parents('.rss_default').find('> a')
            var url = encodeURIComponent($e.attr('href'));
            var title = encodeURIComponent($e.text());
            window.open($this.attr('href')+'/?post='+url+'&title='+title+'&source=bookmarklet','_blank','menubar=no,height=390,width=600,toolbar=no,scrollbars=no,status=no,dialog=1');
            return false;
        });
    }

    function showModal(str){
        closeModal();
        var contenu = str;
        var h = '<div class="modal" style="display:none;"><div class="modal-box">'+contenu+'</div></div>';
        $("#wrapper").append(h);
        $(".modal").fadeIn().css({
            cursor: "pointer"
        }).click(function () {
            $(this).fadeOut();
        });
        $(".modal-box").click(function (e) {
            e.stopPropagation();
            return true
        });
    }

    function closeModal(){
        $(".modal").remove();
    }

    function showNotification(str){
        $("body").append('<div class="popup">'+str+'</div>');
        setTimeout(function(){
            $('.popup').fadeOut('slow', function() {
                $(this).remove();
            });
        }, 1500);
    }

    function saveGrid(){
        var s = gridster.serialize();
        $.ajax({
            data: {
                action: 'save_widgets',
                _p: s,
                token: Token
            },
            dataType: 'html'
        }).done(function(data){
            showNotification(data);
        });
    }

    function refresh_feeds_widgets(){
        var _e = $('.gridster ul > li'), 
            _e_total = _e.length - 1,
            _callback = (arguments && arguments[0] ? arguments[0] : null);

        $.each($('.gridster ul > li'), function(i, el){
            var $this = $(this);
            setTimeout(function(){
               $.ajax({
                    data: {
                        action: 'load_rss_by_id',
                        id: $this.attr('id'),
                        token: Token
                    },
                    dataType: 'html'
                }).done(function(data) {
                    $this.removeClass('loader').find('.grid-content').html(data);
                    if(i === _e_total && _callback){
                        (_callback)();
                    }
                }); 
            },500 + ( i * 500 ));

        });
    }

    function refresh_feed_widget(id_widget){
        var _callback = (arguments && arguments[1] ? arguments[1] : null);
        var $this = $("#"+id_widget);
        $this.addClass('loader').find('.grid-content').html("");
        $.ajax({
            data: {
                action: 'load_rss_by_id',
                id: id_widget,
                token: Token
            },
            dataType: 'html'
        }).done(function(data) {
            $this.removeClass('loader').find('.grid-content').html(data);
            if(_callback){
                (_callback)();
            }
        }); 
    }

    function simple_tooltip(target_items, name){
        $(target_items).each(function(i){
            var $this = $(this);

            var str_date = "";
            if($this.attr('data-pub') !== ""){
                str_date = "<time>Publié le "+$this.attr('data-pub')+"</time>";
            }

            $("body").append("<div class='"+name+"' id='"+name+i+"'><p>"+$this.attr('title')+"</p>"+str_date+"</div>");
            var my_tooltip = $("#"+name+i);
            
            if($this.attr("title") != "" && $this.attr("title") != "undefined" ){
                $this.removeAttr("title").mouseover(function(){
                    my_tooltip.css({opacity:0.8, display:"none"}).fadeIn(400);
                }).mousemove(function(kmouse){
                    var border_top = $(window).scrollTop(); 
                    var border_right = $(window).width();
                    var left_pos;
                    var top_pos;
                    var offset = 20;
                    if(border_right - (offset *2) >= my_tooltip.width() + kmouse.pageX){
                        left_pos = kmouse.pageX+offset;
                        } else{
                        left_pos = border_right-my_tooltip.width()-offset;
                        }
                        
                    if(border_top + (offset *2)>= kmouse.pageY - my_tooltip.height()){
                        top_pos = border_top +offset;
                        } else{
                        top_pos = kmouse.pageY-my_tooltip.height()-offset;
                        }   
                    
                    my_tooltip.css({left:left_pos, top:top_pos});
                }).mouseout(function(){
                        my_tooltip.css({left:"-9999px"});                 
                });
            }
        });
    }

    function removeLinkRSS(el){
        if(confirm("Êtes-vous sûr de vouloir supprimer ce flux ?")){
            var $this = $(el);
            $.ajax({
                data: {
                    action: 'remove_feed',
                    l: $this.attr('data-cible'),
                    c: $this.attr('data-categorie'),
                    token: Token
                },
                dataType: 'html'
            }).done(function(data){
                showNotification(data);
                if($('.sucess').length){
                    $this.parents('.lien_rss').remove();
                }
            });
        }
    }

    function removeCategory(el){
        if(confirm("Êtes-vous sûr de vouloir supprimer cette catégorie et tous les flux associés ?")){
            var $this = $(el);
            $.ajax({
                data: {
                    action: 'remove_category',
                    c: $this.attr('data-categorie'),
                    token: Token
                },
                dataType: 'html'
            }).done(function(data){
                showNotification(data);
                if($('.sucess').length){
                    $('#'+$this.attr('data-categorie')).remove();
                }
            });
        }
    }

    function tryToChangeRefererBeforeOpenLinks(els){
        $(els).click(function(){
            var $this = $(this);

            try {
                delete window.document.referrer;

                window.document.referrer = $this.attr('href');

                Object.defineProperty(
                    document, "referrer", {
                        get : function(){ 
                            return $this.attr('href'); 
                        }
                    }
                );
            }
            catch (e) {}
    
           return true;
        });
    }

    refresh_feeds_widgets(function(){
        var els = ".rss_default > a";
        simple_tooltip(els,"tooltip");
        if($('.context-menu').length){
            contextMenu(els, {menu:".context-menu"});
        }
        if(hideReferer){
            tryToChangeRefererBeforeOpenLinks(els);
        }

    });

    gridster = $(".gridster ul").gridster({
        items: 'li',
        widget_base_dimensions: [200, 120],
        widget_margins: [5, 5],
        resize: {
            enabled: true,
            stop: function(e, ui, $widget) {
                saveGrid();
            }
        },
        draggable: {
            handle: '.grid-header',
            stop: function(e, ui, $el) {
                saveGrid();
            }
        },
        serialize_params: function($w, wgd) {
            return {
                col: wgd.col,
                row: wgd.row,
                size_x: wgd.size_x,
                size_y: wgd.size_y,
                e_id: $(wgd.el).attr('id')

            };
        },
    }).data('gridster');

    $('a.refresh_link').click(function(){
        var $this = $(this);
        refresh_feed_widget($this.attr('data-categorie'), function(){
            var els = ".rss_default > a";
            simple_tooltip(els,"tooltip");
            if($('.context-menu').length){
                contextMenu(els, {menu:".context-menu"});
            }
            if(hideReferer){
                tryToChangeRefererBeforeOpenLinks(els);
            }
        });
        return false;
    });

    $('a.remove').click(function(){
        var el = $(this);
        removeLinkRSS(el);
        return false;
    });

    $('a.add_link').click(function(){
        var $this = $(this);
        var str = '<form id="add_form"><input type="text" required="required" placeholder="Adresse du flux" id="add_flux_url" /><div class="submit_container"><input type="submit" style="" value="Ajouter"></div></form>';
        showModal(str);
        $('#add_form').submit(function(){
            var $o = $('#add_flux_url');
            $.ajax({
                data: {
                    action: 'add_feed',
                    l: $o.val(),
                    c: $this.attr('data-categorie'),
                    token: Token
                },
                dataType: 'json'
            }).done(function(data){
                if(data.s && data.s != ""){
                    showNotification(data.s);
                }
                if(data.j){
                    closeModal();

                    if(data.j['ci'] && data.j['ca'] && data.j['li']){
                        var nlink = '<span class="lien_rss">'+data.j['li']+'<a href="'+data.j['li']+'" title="Ouvrir (nouvel onglet)" class="round view" target="_blank">Ouvrir</a><a href="#" title="Supprimer ce flux" class="round remove" data-cible="'+data.j['ci']+'" data-categorie="'+data.j['ca']+'">Supprimer</a></span>';
                        $('#'+data.j['ca']).find('.grid-content').append(nlink);
                        $('a.remove').click(function(){
                            var el = $(this);
                            removeLinkRSS(el);
                            return false;
                        });
                    }
                }
            });
            return false;
        });

        return false;
    });

    $(".gridoff .grid-content").sortable({
        sort : false,
        group : 'omega',
        onEnd: function (evt){
            var $f = $(evt.from).parents('li').attr('id');
            var $t = $(evt.item).parents('li').attr('id');
            var $l = $(evt.item).find('a.remove').attr('data-cible');
            $.ajax({
                data: {
                    action: 'save_move_flux',
                    f: $f,
                    t: $t,
                    l: $l,
                    token: Token
                },
                dataType: 'html'
            }).done(function(data){
                showNotification(data);
                if($('#'+$f).find('.lien_rss').length === 0){
                    $('#'+$f).find('.grid-content').append('<span class="lien_rss"/>');
                }
            });
        }
    });

    $('a.add_category').click(function(){
        var $this = $(this);

        var colorSelector = '<select name="colors">';
        $.each(_colors, function(k, r){
            colorSelector += '<option value="'+k+'">'+r+'</option>';
        });
        colorSelector += '</select>';

        var str = '<form id="add_form"><input type="text" required="required" placeholder="Nom de la catégorie" id="add_category_name" /> '+colorSelector+'<div class="submit_container"><input type="submit" style="" value="Ajouter"></div></form>';
        showModal(str);
        $('select[name="colors"]').simplecolorpicker({picker: true});

        $('#add_form').submit(function(){
            var $form = $(this);
            var $o = $('#add_category_name');
            $.ajax({
                data: {
                    action: 'add_category',
                    n: $o.val(), 
                    c: $form.find('select[name="colors"]').val(),
                    token: Token
                },
                dataType: 'json'
            }).done(function(data){
                if(data.s && data.s != ""){
                    showNotification(data.s);
                }
                if(data.j){
                    closeModal();

                    if(data.j['ck'] && data.j['cn'] && data.j['cc']){
                        setTimeout(function(){
                            window.location.reload();
                        }, 1000);
                    }
                }
            });
            return false;
        });

        return false;
    });
    
    $('a.edit_category').click(function(){
        var $this = $(this);
        var key = $this.attr('data-categorie');
        var $e = $('#'+key);
        var ncategory = $.trim($e.find('.grid-header .inside').text());
        var ccategory = $e.attr('data-color');

        var colorSelector = '<select name="colors">';
        $.each(_colors, function(k, r){
            colorSelector += '<option value="'+k+'"'+(k === ccategory ? ' selected="selected"':'')+'>'+r+'</option>';
        });
        colorSelector += '</select>';

        var str = '<form id="add_form"><input type="text" value="'+ncategory+'" required="required" placeholder="Nom de la catégorie" id="add_category_name" /> '+colorSelector+'<div class="submit_container"><input type="submit" style="" value="Sauvegarder"></div></form>';
        showModal(str);
        $('select[name="colors"]').simplecolorpicker({picker: true});

        $('#add_form').submit(function(){
            var $form = $(this);
            var $o = $('#add_category_name');
            $.ajax({
                data: {
                    action: 'edit_category',
                    n: $o.val(), 
                    c: $form.find('select[name="colors"]').val(),
                    k: key,
                    token: Token
                },
                dataType: 'json'
            }).done(function(data){
                if(data.s && data.s != ""){
                    showNotification(data.s);
                }
                if(data.j){
                    closeModal();
                    if(data.j['ck'] && data.j['cn'] && data.j['cc']){
                        var $e = $('#'+data.j['ck']);
                        $e.find('.grid-header .inside').text(data.j['cn']);
                        $e.attr('data-color', data.j['cc']);
                        $e.find('.grid-header').css('background-color', data.j['cc']);
                    }
                }
            });
            return false;
        });

        return false;
    });

    $('a.delete_category').click(function(){
        var el = $(this);
        removeCategory(el);
        return false;
    });

    if(navigator.mozApps !== undefined && $('#webapp').length) {
        var manifestUrl = Base + 'assets/froxiss/manifest.webapp';
        var installCheck = navigator.mozApps.checkInstalled(manifestUrl);
        installCheck.onsuccess = function() {
            if (installCheck.result === null) {
                var $area = $('#webapp');
                $area.show();
                var $button = $area.find('a');
                $button.click(function() {
                    var request = window.navigator.mozApps.install(manifestUrl);
                    request.onsuccess = function() {
                        var appRecord = this.result;
                        $area.hide();
                    };
                    request.onerror = function() {
                        alert('Install failed, error: ' + this.error.name);
                    };

                    return false;
                });
            }
        }
    }
});