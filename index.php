<?php

  require_once 'system/initialize.php';

  $top = new Template('top');
  $top->objAgent = $objAgent;
  echo $top->parse();


  $tpl = new Template();
  $tpl->db = $db;
  $tpl->objConfig = $objConfig;

  echo $tpl->parse();
  

  $bottom = new Template('bottom');
  echo $bottom->parse();

?>
