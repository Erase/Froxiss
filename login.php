<?php
  require_once 'system/initialize.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include TL_ROOT.'/system/inc/head.php'; ?>
    </head>
    <body class="login-form <?php echo $objAgent->class;?>">
      <div class="main">
      <div class="login-form">
        <?php 
            $f=file_exists($auto_restrict['path_to_files'].'/auto_restrict_pass.php');
            if($f){
              echo '<h1>Identifiez-vous</h1>';
            }else{
              echo '<h1>Creez votre accès</h1>';
              echo '<span class="small center block">A votre première connexion, le chargement des flux est un peu plus long</span>';
            } 
          ?>
            <div class="head">
              <img src="./assets/froxiss/user.png" alt=""/>
            </div>
          <form method='post'>
              <input type="text" name="login" class="text" value="Login" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Login';}" >
              <input type="password" name="pass" required="required" value="Mot de passe" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Mot de passe';}">
              <div class="submit">
                <input type="submit" value="Connexion" >
            </div>  
            <p>
              <?php if($f){echo '<input id="cookie" type="checkbox" value="cookie" name="cookie"/><label for="cookie">Rester connecté</label>';} ?>
            </p>
          </form>
        </div>
      </div>

      <?php include TL_ROOT.'/system/inc/scripts.php'; ?>
  
    </body>
</html>