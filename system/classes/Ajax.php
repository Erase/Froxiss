<?php

/**
 * Ajax
 *
 * Ajax miscellaneous method
 *
 */
class Ajax
{

	/**
	 * action
	 * @var array
	 */
	private $action;

	/**
	 * Call the method for $callAction
	 *
	 * @param string $callAction The method name
	 * @param array $_params The parameters
	 *
	 * @return mixed The method
	 */
	public function __construct($callAction, $_params = array()){
		$this->action = $callAction;

		if(method_exists($this, $this->action)){
			call_user_func_array(array($this, $this->action), $_params);
		}
		exit;
	}

	private function load_rss_by_id($id = null){
		global $db;
		$str_feed = '<span class="marge"></span>';

		if(!is_null($id)){
			$_data = $db->get('data');
		    if(array_key_exists($id, $_data)){
		      $_links = $_data[$id]['links'];
		      $feeds = new FeedReader($_links);

		      $_favicon = array();
		      foreach($_links as $link){
		      	if(!array_key_exists($link, $_favicon)){
					if(!Favicon::isFaviconExist($link)){
						Favicon::getDistantFavicon($link);
					}

					if(Favicon::isFaviconExist($link)){
						$_k = parse_url($link);
						if(array_key_exists('host', $_k)){
							$_favicon[$_k['host']] = Favicon::getFavicon($link);
						}
					}
		      	}
		      }

		      foreach($feeds->objParseFeed->items as $item){
		      	$fav = null;
		      	if(array_key_exists('base', $item) && $item['base'] !== ""){
			      	$_k = parse_url($item['base']);
			      	if(array_key_exists('host', $_k) && array_key_exists($_k['host'], $_favicon)){
			      		$fav = $_favicon[$_k['host']];
			      	}
    		    }
		      	
		        $str_feed .= '
		          <span class="rss_default'.$item['class'].'">
		            <a '.(Config::get('hideReferer') === true ? 'rel="noreferrer" ': '').'href="'.$item['link'].'" data-pub="'.$item['date_read'].'" target="_blank" title="'.str_replace(array('"', '(Permalink)'), array("'", ''), String::substr($item['description'], Config::get('maxSizeDescription'))).'"'.(!is_null($fav) ? ' class="have_favicon" style="background-image:url(\''.$fav.'\');"' : '').'>'.$item['title'].'</a>
		          	'.(Config::get('shaarliUrl') !== '' ? '
			          	<span class="context-menu">
			          		<ul>
					            <li class="last shaarlier"><a href="'.Config::get('shaarliUrl').'" title="Shaarlier">Shaarlier</a></li>
					        </ul>
			          	</span>' : '').'
		          </span>
		        ';
		      }
		    }
		}

		echo Utilities::minifyHtml($str_feed);

		return;	
	}

	private function add_feed($flux = null, $c_key = null){
		global $db;

		if(!is_null($flux) && !is_null($c_key)){
			if(strpos($flux, 'http') === false){
				$flux = 'http://'.$flux;
			}
			//~ Test feed
			$objFeed = new SimplePie();
			$objFeed->set_feed_url($flux);
			$objFeed->init();
			$objFeed->handle_content_type();

			if (!$objFeed->error()){
			    //~ No feed error, its ok :)
			    $link = $objFeed->subscribe_url();
				$_d = $db->getData()['data'];
				if(array_key_exists($c_key, $_d)){
					if(!in_array($link, $_d[$c_key]['links'])){
						array_push($_d[$c_key]['links'], $link);
						$db->set('data', $_d);
						$db->save();

						Favicon::getDistantFavicon($link);

						exit(json_encode(
							array(
								's'	=>	'<p class="sucess">Flux ajouté</p>',
								'j'	=>	array(
									'ci'	=>	md5($link),
									'ca'	=>	$c_key,
									'li'	=>	$link
								)
							)
						));
					}
					exit(json_encode(
						array(
							's'	=>	'<p class="error">Ce flux existe déjà dans cette catégorie</p>',
							'j'	=>	''
						)
					));
				}
			}
		}
		exit(json_encode(
			array(
				's'	=>	'<p class="error">Une erreur est suvenue lors de l\'ajout du flux.</p>',
				'j'	=> 	''
			)
		));
	}


	private function remove_feed($c_flux, $c_key){
		global $db;

		$_d = $db->getData()['data'];
		if(array_key_exists($c_key, $_d)){
			foreach($_d[$c_key]['links'] as $o => $link){
				if(md5($link) === $c_flux){
					unset($_d[$c_key]['links'][$o]);
					$db->set('data', $_d);
					$db->save();	
					exit('<p class="sucess">Flux supprimé</p>');
				}
			}
		}

		exit('<p class="error">Une erreur est suvenue lors de la suppression du flux.</p>');
	}

	private function save_widgets($_widgets = array()){
		global $db;

		if(count($_widgets)){
			$_d = $db->getData()['data'];	//	()[]

			foreach($_widgets as $w){
				if(array_key_exists($w['e_id'], $_d)){
					$_d[$w['e_id']]['grid'] = array(
						'row'		=>	$w['row'],
						'col'		=>	$w['col'],
						'size_x'	=>	$w['size_x'],
						'size_y'	=>	$w['size_y']
					);
				}
			}

			$db->set('data', $_d);
			$db->save();	

			exit('<p class="success">Configuration sauvegardée.</p>');
			
		}

		exit('<p class="error">Une erreur est suvenue lors de la sauvegarde de vos paramètres.</p>');
	}

	private function save_move_flux($from = null, $to = null, $link = null){
		global $db;

		if(!is_null($from) && !is_null($to) && !is_null($link)){
			$_d = $db->getData()['data'];	
			$str_link = null;

			if(array_key_exists($from, $_d) && count($_d[$from]['links'])){
				foreach($_d[$from]['links'] as $k => $links){
					if(md5($links) === $link){
						$str_link = $links;
						unset($_d[$from]['links'][$k]);
						break;
					}
				}
			}

			if(!is_null($str_link) && array_key_exists($to, $_d) && !in_array($str_link, $_d[$to]['links'])){
				array_push($_d[$to]['links'], $str_link);
			}

			$db->set('data', $_d);
			$db->save();	

			exit('<p class="success">Configuration sauvegardée.</p>');
			
		}

		exit('<p class="error">Une erreur est suvenue lors de la sauvegarde de vos paramètres.</p>');
	}

	private function add_category($category = null, $color = null){
		global $db;

		if(!is_null($category) && !is_null($color)){
			$_d = $db->getData()['data'];


			if($color != '' && array_key_exists($color, $GLOBALS['TL_CONFIG']['colorPicker']) && $category !== ''){
				//~ Generate key of category
				$k = 'a'.md5($category.time());

				if(!array_key_exists($k, $_d)){
					$_d[$k] = array(
						'name'	=>	$category,
						'color'	=>	$color,
						'links'	=>	array(),
						'grid'	=>	array(
							'row'		=>	1,
							'col'		=>	1,
							'size_x'	=>	1,
							'size_y'	=>	1
						)
					);

					$db->set('data', $_d);
					$db->save();	
					exit(json_encode(
						array(
							's'	=>	'<p class="sucess">Catégorie ajoutée</p>',
							'j'	=>	array(
								'ck'	=>	$k,
								'cn'	=>	$category,
								'cc'	=>	$color
							)
						)
					));
				}
			}
		}

		exit(json_encode(
			array(
				's'	=>	'<p class="error">Une erreur est suvenue lors de la création de la catégorie.</p>',
				'j'	=> 	''
			)
		));
	}

	private function remove_category($c_key){
		global $db;

		$_d = $db->getData()['data'];
		if(array_key_exists($c_key, $_d)){
			unset($_d[$c_key]);
			$db->set('data', $_d);
			$db->save();	
			exit('<p class="sucess">Catégorie supprimée</p>');
		}

		exit('<p class="error">Une erreur est suvenue lors de la suppression de la catégorie.</p>');
	}

	private function edit_category($category = null, $color = null, $c_key = null){
		global $db;

		if(!is_null($category) && !is_null($color) && !is_null($c_key)){
			$_d = $db->getData()['data'];

			if($color != '' && array_key_exists($color, $GLOBALS['TL_CONFIG']['colorPicker']) && $category !== '' && array_key_exists($c_key, $_d)){
				$_d[$c_key]['name'] = $category;
				$_d[$c_key]['color'] = $color;

				$db->set('data', $_d);
				$db->save();	
				exit(json_encode(
					array(
						's'	=>	'<p class="sucess">Catégorie éditée</p>',
						'j'	=>	array(
							'ck'	=>	$c_key,
							'cn'	=>	$category,
							'cc'	=>	$color
						)
					)
				));
			}
		}

		exit(json_encode(
			array(
				's'	=>	'<p class="error">Une erreur est suvenue lors de l\'édition de la catégorie.</p>',
				'j'	=> 	''
			)
		));
	}

}
