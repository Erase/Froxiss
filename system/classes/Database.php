<?php

/**
 * Database
 *
 * Read, store and search in database
 *
 */
class Database
{

	/**
	 * arrData
	 * @var array
	 */
	public $arrData;


	/**
	 * Store all database items in array
	 *
	 */
	public function __construct()
	{
		$this->arrData=(file_exists(Config::get('DataStoreFile')) ? unserialize(gzinflate(base64_decode(substr(file_get_contents(Config::get('DataStoreFile')),strlen(Config::get('STORE_PREFIX')),-strlen(Config::get('STORE_SUFFIX')))))) : array() );
	}


	/**
	 * Return a table data
	 *
	 * @param string $strKey The variable name
	 *
	 * @return mixed The variable value
	 */
	public function get($strKey)
	{
		return $this->arrData[$strKey];
	}

	/**
	 * Save data in files
	 *
	 */
	public function save()
	{
		return file_put_contents(Config::get('DataStoreFile'), Config::get('STORE_PREFIX').base64_encode(gzdeflate(serialize($this->arrData))).Config::get('STORE_SUFFIX'));
	}

	/**
	 * Set a data variable
	 *
	 * @param string $strKey   The variable name
	 * @param mixed  $varValue The variable value
	 */
	public function set($strKey, $varValue)
	{
		$this->arrData[$strKey] = $varValue;
	}

	/**
	 * Remove a data variable
	 *
	 * @param string $strKey The variable name
	 */
	public function remove($strKey)
	{
		unset($this->arrData[$strKey]);
	}


	/**
	 * Return the data as array
	 *
	 * @return array The session data
	 */
	public function getData()
	{
		return (array) $this->arrData;
	}
	
}
