<?php

/**
 * Favicon
 *
 * Manipulation of favicon media
 *
 *
 */
class Favicon
{


	/**
	 * Prevent direct instantiation (Singleton)
	 */
	protected function __construct() {}


	public static function getDistantFavicon($flux){
		if(function_exists('curl_version')){
			$_url = parse_url($flux);
			if(array_key_exists('host', $_url) && $_url['host'] !== ""){
				$cible = $_url['host'].(array_key_exists('path', $_url) ? $_url['path'] : '');
				$favicon = new FaviconDownloader($cible);
				if($favicon->icoExists){
					$filename = Config::get('dataStorePath').String::simpleCrypt($flux).'.'.$favicon->icoType;
					file_put_contents($filename, $favicon->icoData);

					//~ Download image for base64 encode && remove
					$fp = fopen(Config::get('dataStorePath').'ico_'.String::simpleCrypt($flux), 'c');
					fwrite($fp, Config::get('STORE_PREFIX').self::base64_encode_image($filename, $favicon->icoType).Config::get('STORE_SUFFIX'));
					fclose($fp);

					unlink($filename);
				}
			}
		}
	}

	public static function isFaviconExist($flux){
		return file_exists(Config::get('dataStorePath').'ico_'.String::simpleCrypt($flux));
	}

	public static function getFavicon($flux){
		return substr(
			file_get_contents(
				Config::get('dataStorePath').'ico_'.String::simpleCrypt($flux)
			),
			strlen(
				Config::get('STORE_PREFIX')
			),
			-strlen(
				Config::get('STORE_SUFFIX')
			)
		);
	}

	//~ http://php.net/manual/fr/function.base64-encode.php#105200
	public static function base64_encode_image($filename, $filetype) {
	    if($filename){
	        $imgbinary = fread(fopen($filename, "r"), filesize($filename));
	        return 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
	    }
	}
	
}