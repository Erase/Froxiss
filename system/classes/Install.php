<?php

/**
 * Install
 * Run at first launch
 */
class Install
{	
	
	public static function initialize($_params = array()){
		//~ Verif lock
		if(!file_exists(TL_ROOT.'/data/.lock')){
			//~ Create dir & files
			if(self::verifFolder() && self::createFolder() && self::createFiles()){
				//~ Username
				$usr = self::setFirstUsername($_params);
				if($usr){
					//~ Db
					self::setFirstDatabase($usr);

					//~ Lock install
					if(!file_put_contents(TL_ROOT.'/data/.lock', time())){
						echo '<p class="error">Error : File ".lock" creation</p>';
						exit;
					}
				}
			}
		}
	}

	public static function createFiles(){
		if(!file_put_contents(TL_ROOT.'/system/logs/error.log', 'Installation at '.time())){
			echo '<p class="error">Error : File "error.log" creation</p>';
			exit;
		}
		if(!file_put_contents(TL_ROOT.'/system/config/localconfig.php', '<?php')){
			echo '<p class="error">Error : File "localconfig" creation</p>';
			exit;
		}
		return true;
	}

	public static function createFolder(){
		if(!mkdir(TL_ROOT.'/system/logs',0777)){
			echo '<p class="error">Error : folder "logs" creation</p>';
			exit;
		} 

		if(!mkdir(TL_ROOT.'/system/cache',0777)){
			echo '<p class="error">Error : folder "cache" creation</p>';
			exit;
		}

		return true;
	}

	public static function verifFolder(){
		if(!is_writable(TL_ROOT.'/data')){
			echo '<p class="error">Error : folder "data" must be writable</p>';
			exit;
		}
		if(!is_writable(TL_ROOT.'/system')){
			echo '<p class="error">Error : folder "system" must be writable</p>';
			exit;
		}
		if(!is_writable(TL_ROOT.'/system/config')){
			if(!chmod(TL_ROOT.'/system/config',0777)){
				echo '<p class="error">Error : folder "system/config" must be writable</p>';
				exit;
			}
		}

		return true;
	}

	public static function setFirstUsername($_p = array()){
		if(!array_key_exists('login', $_p) OR $_p['login'] === ""){
			echo '<p class="error">Error : create user</p>';
			exit;
		}

		$objConfig = Config::getInstance();
		$objConfig->read();
		$objConfig->persist('username', $_p['login']);
        $objConfig->save();
        return $_p['login'];
	}

	public static function setFirstDatabase($username){
		Config::set('dataStorePath', TL_ROOT.'/data/');
		Config::set('DataStoreFile', Config::get('dataStorePath').String::simpleCrypt($username));

		//~ Set database reference
		$db = new Database();
		$db->set('data', array(
			'a'.md5('Copains') => array(
				'name'	=>	'Copains',
				'color'	=>	'#f2b736',
				'links'	=>	array(
					'http://sebsauvage.net/links/?do=rss',
					'http://shaarli.warriordudimanche.net/?do=rss',
					'http://links.green-effect.fr/?do=flux_rss'
				),
				'grid'	=>	array(
					'row'		=>	1,
					'col'		=>	1,
					'size_x'	=>	2,
					'size_y'	=>	2
				)
			),
			'a'.md5('Infos') => array(
				'name'	=>	'Infos',
				'color'	=>	'#c5523f',
				'links'	=>	array(
					'http://www.lemonde.fr/m-actu/rss_full.xml'
				),
				'grid'	=>	array(
					'row'		=>	1,
					'col'		=>	2,
					'size_x'	=>	1,
					'size_y'	=>	2
				)
			),
			'a'.md5('Actu IT') => array(
				'name'	=>	'Actu IT',
				'color'	=>	'#499255',
				'links'	=>	array(
					'http://www.developpez.com/index/rss',
					'http://www.nextinpact.com/rss/news.xml'
				),
				'grid'	=>	array(
					'row'		=>	1,
					'col'		=>	2,
					'size_x'	=>	1,
					'size_y'	=>	2
				)
			)
		));
		$db->save();
	}

}