<?php

/**
 * Menu & navigation
 *
 * The class provides an interface to generate menu and navigation.
 *
 *
 */
class Menu
{


	/**
	 * Prevent direct instantiation (Singleton)
	 */
	protected function __construct() {}


	/**
	 * Generate ul/li navigation
	 *
	 * @param (optional) array $_links   Array of link (default = Config::get('menuItems'))
	 * @param (optional) array $_attr    Array of attributs for <nav/>
	 */
	public static function generate($_links = array(), $_attr = array()){
		if(!count($_links) || is_null($_links)){
			$_tmp = Config::get('pagesItems');
			$_links = array();
			foreach($_tmp as $k=>$link){
				if(array_key_exists('showMenu', $link) && $link['showMenu']){
					$_links[$k] = $link;
				}
			}
		}
		
		array_walk($_attr, create_function('&$i,$k','$i=" $k=\"$i\"";'));
		$attr = implode($_attr,"");
		
		$ref 		= 'b'.md5(time());
		$i 			= 0;
		$last 		= count($_links);
		$str_menu 	= '<nav'.$attr.'><input class="menu-nav" type="checkbox" id="'.$ref.'" checked="checked" /><label for="'.$ref.'"></label><div class="nav-content"><ul>';

		foreach($_links as $title => $_p){
			$u = (array_key_exists('alias', $_p) && $_p['alias'] !== '' ? $_p['alias'] : standardize($title));
			if(empty($_p['url'])){
				$_p['url'] = 'index.php?do='.$u;
			}

			$class = trim((($i == 0) ? ' first' : '') . (($i == $last) ? ' last' : '') . ((($i % 2) == 0) ? ' even' : ' odd') . (Menu::isCurrentLink($u) ? ' active' : ''));
			$class_url = null;
			if(array_key_exists('class', $_p)){
				$class_url = trim($class.' '.$_p['class']);
			}
			
			$str_menu .= '
				<li class="'.$class.'">
					<a href="'.Environment::get('base').$_p['url'].'" title="'.$title.'" class="'.(!is_null($class_url) ? $class_url : $class).'">
						'.$title.'
					</a>
				</li>
			';
			$i++;
		}

		return $str_menu.'</ul></div></nav>';
	}


	/**
	 * Determin if link is the current URL
	 *
	 * @param string $href   URL of link
	 * @return bol 
	 */
	public static function isCurrentLink($href){
		$href = str_replace(Environment::get('base'), '', $href);
		return (strpos(Environment::get('request'), $href) OR ($href === "accueil" && Environment::get('request') === "index.php"));
	}
	
}