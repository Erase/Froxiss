<?php

/**
 * OPML
 *
 * Generate an OPML file with lib_opml.php
 */
class OPML
{

	static public function import(){
		global $db;

	    $uploadFile = Config::get('dataStorePath') . $_FILES['opml_file']['name'];
	    $extension = end(explode(".", $_FILES["opml_file"]["name"]));
	    if($extension === "xml"){	    
		    if(move_uploaded_file($_FILES['opml_file']['tmp_name'], $uploadFile)){
		        $_opml = libopml_parse_file($uploadFile);
		        if($_opml && gettype($_opml) == 'array' && array_key_exists('body', $_opml)){

		        	$_data = $db->get('data');
		        	$_categories = array();

		        	foreach($_opml['body'] as $categorie){
		        		$k = null;
		        		foreach($_data as $key => $c){
		        			if($c['name'] == $categorie['text']){
		        				$k = $key;
		        				break;
		        			}
		        		}
		        		if(is_null($k)){
		        			$k = 'a'.md5($categorie['text'].time());
		        			$_data[$k] = array(
								'name'	=>	$categorie['text'],
								'color'	=>	'#7bd148',
								'links'	=>	array(),
								'grid'	=>	array(
									'row'		=>	1,
									'col'		=>	1,
									'size_x'	=>	1,
									'size_y'	=>	1
								)
							);
		        		}

		        		if(array_key_exists($k, $_data) && array_key_exists('links', $_data[$k])){
		        			foreach($categorie['@outlines'] as $_links){
			        			if(!in_array($_links['xmlUrl'], $_data[$k]['links'])){
				        			array_push($_data[$k]['links'], $_links['xmlUrl']);
				        		}
			        		}
		        		}
		        	}

		        	$db->set('data', $_data);
					$db->save();

		        }
				
				unlink($uploadFile);
		    }
		}
	}

	static public function export(){
		global $db;

		$_data = $db->get('data');
	    $_opml = array(
	        'version'   =>  '0.2',
	        'head'      =>  array(
	            'title'         =>  'Export OPML du '.Config::get('websiteTitle').' de '.Config::get('username'),
	            'dateCreated'   =>  date(DATE_RFC2822)
	        ),
	        'body'      =>  array()
	    );
	    foreach($_data as $key => $categorie){
	        if(array_key_exists('links', $categorie)){
	            $_outlines = array();
	            foreach($categorie['links'] as  $link){
	                $_url = parse_url($link);
	                $text = (array_key_exists('host', $_url) ? 'Flux de '.$_url['host'] : '');
	                array_push($_outlines, array(
	                    'text'      =>  $text,
	                    'type'      =>  'rss',
	                    'xmlUrl'    =>  $link
	                ));
	            }

	            array_push($_opml['body'], array(
	                'text'      =>  $categorie['name'],
	                '@outlines' =>  $_outlines
	            ));
	        }   
	    } 

	    self::render(libopml_render($_opml));
	}

	static private function render($opml_string){
		ob_end_clean(); 

		ob_clean();

		header("Content-Disposition: filename=\"export_opml.xml\"");
		header("Content-Type: application/force-download");
		header("Content-Transfer-Encoding: text/xml\n");
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
		header("Expires: 0");

		flush(); 

		echo $opml_string;
		exit;
	}
	
}