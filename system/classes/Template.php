<?php

/**
 * Template
 * Parses and outputs template files
 */
class Template
{	
	/**
	 * Content type
	 * @var string
	 */
	protected $strContentType;

	/**
	 * Template data
	 * @var array
	 */
	protected $arrData = array();


	/**
	 * Create a new template object
	 *
	 * @param string $strTemplate    The template name
	 * @param string $strContentType The content type (defaults to "text/html")
	 */
	public function __construct($strTemplate='', $strContentType='text/html', $strFormat='html5'){
		$this->strContentType = $strContentType;
		$this->strFormat = $strFormat;
		
		if($strTemplate === ''){
			$strTemplate = $this->determineTemplate();
		}

		$this->strTemplate = $strTemplate;
	}

	/**
	 * Set the template data from an array
	 *
	 * @param array $arrData The data array
	 */
	public function setData($arrData)
	{
		$this->arrData = $arrData;
	}


	/**
	 * Return the template data as array
	 *
	 * @return array The data array
	 */
	public function getData()
	{
		return $this->arrData;
	}


	/**
	 * Set the template name
	 *
	 * @param string $strTemplate The template name
	 */
	public function setName($strTemplate)
	{
		$this->strTemplate = $strTemplate;
	}


	/**
	 * Return the template name
	 *
	 * @return string The template name
	 */
	public function getName()
	{
		return $this->strTemplate;
	}


	/**
	 * Set the output format
	 *
	 * @param string $strFormat The output format
	 */
	public function setFormat($strFormat)
	{
		$this->strFormat = $strFormat;
	}


	/**
	 * Return the output format
	 *
	 * @return string The output format
	 */
	public function getFormat()
	{
		return $this->strFormat;
	}


	/**
	 * Print all template variables to the screen using print_r
	 */
	public function showTemplateVars()
	{
		echo "<pre>\n";
		print_r($this->arrData);
		echo "</pre>\n";
	}


	/**
	 * Print all template variables to the screen using var_dump
	 */
	public function dumpTemplateVars()
	{
		dump($this->arrData);
	}


	/**
	 * Determin template by "do" params
	 */
	public function determineTemplate()
	{
		$page = Input::get('do');
		if($page && !empty($page)){
			$_pages = Config::get('pagesItems'); 
			foreach($_pages as $k=>$p){
				if((standardize($k) === $page OR $p['alias'] === $page) && !empty($p['tpl'])){
					if(!$this->getTemplate($p['tpl'], $this->strFormat)){
						return '404';
					}else{
						return $p['tpl'];
					}
				}
			}
		}else{
			return 'index';
		}

		return '404';
	}


	/**
	 * Parse the template file and return it as string
	 *
	 * @return string The template markup
	 */
	public function parse()
	{
		if ($this->strTemplate == '')
		{
			return '';
		}

		$strBuffer = '';

		// Start with the template itself
		$this->strParent = $this->strTemplate;

		// Include the parent templates
		while ($this->strParent !== null)
		{
			$strCurrent = $this->strParent;
			$strParent = $this->strDefault ?: $this->getTemplate($this->strParent, $this->strFormat);

			// Reset the flags
			$this->strParent = null;
			$this->strDefault = null;

			ob_start();
			include $strParent;

			// Capture the output of the root template
			if ($this->strParent === null)
			{
				$strBuffer = ob_get_contents();
			}

			ob_end_clean();
		}

		// Add start and end markers in debug mode
		if (\Config::get('debugMode'))
		{
			$strRelPath = str_replace(TL_ROOT . '/', '', $this->getTemplate($this->strTemplate, $this->strFormat));
			$strBuffer = "\n<!-- TEMPLATE START: $strRelPath -->\n$strBuffer\n<!-- TEMPLATE END: $strRelPath -->\n";
		}
		
		return Utilities::minifyHtml($strBuffer);
	}



	/**
	 * Find a particular template file and return its path
	 *
	 * @param string $strTemplate The name of the template
	 * @param string $strFormat   The file extension
	 *
	 * @return string The path to the template file
	 *
	 */
	public static function getTemplate($strTemplate, $strFormat='html5')
	{
	
		if(file_exists(TL_ROOT.'/tpl/'.$strTemplate.'.'.$strFormat)){
			return TL_ROOT.'/tpl/'.$strTemplate.'.'.$strFormat;
		}
		
		return false;
	}
}