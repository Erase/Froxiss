<?php

/**
 * Theme
 *
 * The class provides an interface to theme gestion
 *
 *
 */
class Theme
{

	/**
	 * Cache
	 * @var array
	 */
	protected static $arrCache = array();


	/**
	 * Return an class variable
	 *
	 * @param string $strKey The variable name
	 *
	 * @return mixed The variable value
	 */
	public static function get($strKey)
	{
		if (isset(static::$arrCache[$strKey]))
		{
			return static::$arrCache[$strKey];
		}

		if (in_array($strKey, get_class_methods('Theme')))
		{
			static::$arrCache[$strKey] = static::$strKey();
		}

		return static::$arrCache[$strKey];
	}


	/**
	 * Set an environment variable
	 *
	 * @param string $strKey   The variable name
	 * @param mixed  $varValue The variable value
	 */
	public static function set($strKey, $varValue)
	{
		static::$arrCache[$strKey] = $varValue;
	}


	/**
	 * Get theme name
	 */
	public static function getTheme(){
		if(array_key_exists('theme', $GLOBALS['TL_CONFIG']) && Config::get('theme') !== ''){
			if(is_dir(self::getThemeRoot().Config::get('theme'))){
				return self::getThemeRoot().Config::get('theme');
			}
		}
		return false;
	}


	/**
	 * Get theme path root
	 */
	public static function getThemeRoot(){
		return TL_ROOT.'/assets/themes/';
	}


	/**
	 * Generate style 
	 */
	public static function generateStyle(){
		$theme = self::getTheme();
		$strFolder = $theme.'/css';

		if($theme && is_dir($strFolder) && !self::isEmpty($strFolder)){

			$arrFiles = scan($strFolder, false);

			if(Config::get('minifyMarkup') && !Config::get('debugMode')){
				echo "<style>";ob_start('Utilities::minifyCss');
			}

			foreach ($arrFiles as $strFile)
			{
				if (is_file($strFolder . '/' . $strFile))
				{
					if(Config::get('minifyMarkup') && !Config::get('debugMode')){
						include $strFolder.'/'.$strFile;
					}else{
						
						echo '<link rel="stylesheet" type="text/css" href="./'.(str_replace(TL_ROOT.'/', '', $strFolder . '/' . $strFile)).'">';
						
					}
				}
			}

			if(Config::get('minifyMarkup') && !Config::get('debugMode')){
				if(Config::get('customCSS') && Config::get('customCSS') !== ''){
					echo Utilities::minifyCss(Config::get('customCSS'));
				}
				ob_end_flush();echo "</style>";
			}

			if(Config::get('customCSS') && Config::get('customCSS') !== ''){
				echo "<style>";
				echo Utilities::minifyCss(Config::get('customCSS'));
				echo "</style>";
			}
		}
	}


	/**
	 * List theme folder
	 */
	public static function getListe(){

		$strFolder = self::getThemeRoot();
		
		if(is_dir($strFolder) && !self::isEmpty($strFolder)){
			$arrFiles = scan($strFolder, false);
			$arrDir   = array();
			foreach ($arrFiles as $strFile)
			{
				if(is_dir($strFolder . '/' . $strFile)){
					array_push($arrDir, $strFile);
				}
			}
			return $arrDir;
		}

		return false;
	}


	/**
	 * Get base script files
	 **/
	public static function getBaseScript(){
		$strFolder = TL_ROOT.'/assets/froxiss/js';
		$arrFiles  = scan($strFolder, false);
		$strReturn = '';

		foreach ($arrFiles as $strFile)
		{
			if (is_file($strFolder . '/' . $strFile))
			{
				$strReturn .= '<script src="./'.(str_replace(TL_ROOT.'/', '', $strFolder . '/' . $strFile)).'"></script>';
			}
		}

		return $strReturn;
	}


	/**
	* Generate script
	*/
	public static function generateScript(){
		$theme     = self::getTheme();
		$strFolder = $theme.'/js';

		echo self::getBaseScript();

		if($theme && is_dir($strFolder) && !self::isEmpty($strFolder)){

			$arrFiles = scan($strFolder, false);

			foreach ($arrFiles as $strFile)
			{
				if (is_file($strFolder . '/' . $strFile))
				{
					echo '<script src="./'.(str_replace(TL_ROOT.'/', '', $strFolder . '/' . $strFile)).'"></script>';
				}
			}
		}
	}

	/**
	 * Return true if the folder is empty
	 *
	 * @return boolean True if the folder is empty
	 */
	public static function isEmpty($strFolder)
	{
		return (count(scan($strFolder, true)) < 1);
	}
	
}