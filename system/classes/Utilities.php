<?php

/**
 * Utilities
 * Miscellaneous utilities
 */
class Utilities
{	
	
	/**
	 * Minify the HTML markup preserving pre, script, style and textarea tags
	 *
	 * @param string $strHtml The HTML markup
	 *
	 * @return string The minified HTML markup
	 */
	public function minifyHtml($strHtml)
	{
		/*
			Possible utilisation : 
			ob_start();
			// file content
			$content = ob_get_contents();
			ob_end_clean();
			echo Utilities::minifyHtml($contents);
		*/

		// The feature has been disabled
		if (!\Config::get('minifyMarkup') || \Config::get('debugMode'))
		{
			return $strHtml;
		}

		// Split the markup based on the tags that shall be preserved
		$arrChunks = preg_split('@(</?pre[^>]*>)|(</?script[^>]*>)|(</?style[^>]*>)|( ?</?textarea[^>]*>)@i', $strHtml, -1, PREG_SPLIT_DELIM_CAPTURE|PREG_SPLIT_NO_EMPTY);

		$strHtml = '';
		$blnPreserveNext = false;
		$blnOptimizeNext = false;

		// Recombine the markup
		foreach ($arrChunks as $strChunk)
		{
			if (strncasecmp($strChunk, '<pre', 4) === 0 || strncasecmp(ltrim($strChunk), '<textarea', 9) === 0)
			{
				$blnPreserveNext = true;
			}
			elseif (strncasecmp($strChunk, '<script', 7) === 0 || strncasecmp($strChunk, '<style', 6) === 0)
			{
				$blnOptimizeNext = true;
			}
			elseif ($blnPreserveNext)
			{
				$blnPreserveNext = false;
			}
			elseif ($blnOptimizeNext)
			{
				$blnOptimizeNext = false;

				// Minify inline scripts
				$strChunk = str_replace(array("/* <![CDATA[ */\n", "<!--\n", "\n//-->"), array('/* <![CDATA[ */', '', ''), $strChunk);
				$strChunk = preg_replace(array('@(?<![:\'"])//(?!W3C|DTD|EN).*@', '/[ \n\t]*(;|=|\{|\}|\[|\]|&&|,|<|>|\',|",|\':|":|: |\|\|)[ \n\t]*/'), array('', '$1'), $strChunk);
				$strChunk = trim($strChunk);
			}
			else
			{
				$arrReplace = array
				(
					'/\n ?\n+/'                   => "\n",    // Convert multiple line-breaks
					'/^[\t ]+</m'                 => '<',     // Remove tag indentation
					'/>\n<(a|input|select|span)/' => '> <$1', // Remove line-breaks between tags
					'/([^>])\n/'                  => '$1 ',   // Remove line-breaks of wrapped text
					'/  +/'                       => ' ',     // Remove redundant whitespace characters
					'/\n/'                        => '',      // Remove all remaining line-breaks
					'/ <\/(div|p)>/'              => '</$1>'  // Remove spaces before closing DIV and P tags
				);

				$strChunk = str_replace("\r", '', $strChunk);
				$strChunk = preg_replace(array_keys($arrReplace), array_values($arrReplace), $strChunk);
				$strChunk = trim($strChunk);
			}

			$strHtml .= $strChunk;
		}

		return $strHtml;
	}


	public function minifyCss($strCss) 
    {
    	$strCss = str_replace('../', 'assets/themes/'.Config::get('theme').'/', $strCss);
    	$strCss = preg_replace( '!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $strCss);
    	$strCss = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $strCss);
	    $strCss = str_replace(["\r\n","\r","\n","\t",'  ','    ','     '], '', $strCss);
	    $strCss = preg_replace(['(( )+{)','({( )+)'], '{', $strCss);
	    $strCss = preg_replace(['(( )+})','(}( )+)','(;( )*})'], '}', $strCss);
	    $strCss = preg_replace(['(;( )+)','(( )+;)'], ';', $strCss);	
	    
        return $strCss;
    }


    /**
	 * Reload the current page
	 */
	public static function reload()
	{	
		$strLocation = \Environment::get('uri');

		if (headers_sent())
		{
			echo '<script type="text/javascript">';
	        echo 'window.location.href="'.$strLocation.'";';
	        echo '</script>';
	        echo '<noscript>';
	        echo '<meta http-equiv="refresh" content="0;url='.$strLocation.'" />';
	        echo '</noscript>';
			exit;
		}

		// Ajax request
		if (\Environment::get('isAjaxRequest'))
		{
			header('HTTP/1.1 204 No Content');
			header('X-Ajax-Location: ' . $strLocation);
		}
		else
		{
			header('HTTP/1.1 303 See Other');
			header('Location: ' . $strLocation);
		}

		exit;
	}


	/**
	 * Redirect to another page
	 *
	 * @param string  $strLocation The target URL
	 * @param integer $intStatus   The HTTP status code (defaults to 303)
	 */
	public static function redirect($strLocation, $intStatus=303)
	{
		if (headers_sent())
		{
			echo '<script type="text/javascript">';
	        echo 'window.location.href="'.$strLocation.'";';
	        echo '</script>';
	        echo '<noscript>';
	        echo '<meta http-equiv="refresh" content="0;url='.$strLocation.'" />';
	        echo '</noscript>';
	        exit;
		}

		$strLocation = str_replace('&amp;', '&', $strLocation);

		// Make the location an absolute URL
		if (!preg_match('@^https?://@i', $strLocation))
		{
			$strLocation = \Environment::get('base') . $strLocation;
		}

		// Ajax request
		if (\Environment::get('isAjaxRequest'))
		{
			header('HTTP/1.1 204 No Content');
			header('X-Ajax-Location: ' . $strLocation);
		}
		else
		{
			// Add the HTTP header
			switch ($intStatus)
			{
				case 301:
					header('HTTP/1.1 301 Moved Permanently');
					break;

				case 302:
					header('HTTP/1.1 302 Found');
					break;

				case 303:
					header('HTTP/1.1 303 See Other');
					break;

				case 307:
					header('HTTP/1.1 307 Temporary Redirect');
					break;
			}

			header('Location: ' . $strLocation);
		}

		exit;
	}

	public static function launchInstall($_params = array()){
		include TL_ROOT . '/system/classes/Install.php';
		Install::initialize($_params);
	}

	public static function getHellowMessage(){
		$_messages = Config::get('HellowMessage');
		$_messages = shuffle_assoc($_messages);
		foreach($_messages as $message){
			if($message['condition']){
				$var = array();
				foreach($message['var'] as $val){
					if(Config::get($val)){
						$var[$val] = Config::get($val);
					}
				}
				return sprintf2($message['message'], $var);
			}
		}
	}

}