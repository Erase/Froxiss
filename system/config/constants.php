<?php

	//~ Core version
	define('VERSION', '0.9');
	define('BUILD', '1');
	define('LONG_TERM_SUPPORT', false);


	//~ Plugin & lib versions
	define('JQUERY', '1.10.2');			// js
	define('SIZZLE', '1.10.2');			// js
	define('GRIDSTER', '0.5.6');		// js
	define('SORTABLE', '1.3.0');		// js
	define('COLORPICKER', '0.3.0');		// js

	define('SIMPLEPIE', '1.3.1');		// php
	define('FAVICONDOWNLOADER', '1');	// php
	define('LIB_OPML', '0.2');			// php
	define('AUTO_RESTRICT', '3.3');		// php
