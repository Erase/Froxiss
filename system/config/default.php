<?php


##############################################################
#                                                            #
#  DO NOT CHANGE ANYTHING HERE! USE THE LOCAL CONFIGURATION  #
#  FILE localconfig.php TO MODIFY THE CONFIGURATION!  		 #
#                                                            #
##############################################################


/**
 * -------------------------------------------------------------------------
 * GENERAL SETTINGS
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_CONFIG']['websiteTitle']   = 'Froxiss - Minimalist RSS Reader';
$GLOBALS['TL_CONFIG']['characterSet']   = 'utf-8';
$GLOBALS['TL_CONFIG']['adminEmail']     = ''; //~ Non utilisé pour l'instant, pour permettre la mise en place de récap par cron
$GLOBALS['TL_CONFIG']['shaarliUrl']     = '';
$GLOBALS['TL_CONFIG']['displayErrors']  = false;
$GLOBALS['TL_CONFIG']['logErrors']      = true;
$GLOBALS['TL_CONFIG']['minifyMarkup']   = false;
$GLOBALS['TL_CONFIG']['username']		= 'admin';
$GLOBALS['TL_CONFIG']['debugMode']		= false;
$GLOBALS['TL_CONFIG']['theme']			= 'default';
$GLOBALS['TL_CONFIG']['customCSS']		= '';
$GLOBALS['TL_CONFIG']['hideReferer']	= true;



/**
 * -------------------------------------------------------------------------
 * DATE AND TIME SETTINGS
 * -------------------------------------------------------------------------
 *
 *   datimFormat = show date and time
 *   dateFormat  = show date only
 *   timeFormat  = show time only
 *   timeZone    = the server's default time zone
 *
 * See PHP function date() for more information.
 */
$GLOBALS['TL_CONFIG']['datimFormat']      = 'd-m-Y H:i';
$GLOBALS['TL_CONFIG']['dateFormat']       = 'd-m-Y';
$GLOBALS['TL_CONFIG']['timeFormat']       = 'H:i';
$GLOBALS['TL_CONFIG']['timeZone']         = (!is_null(ini_get('date.timezone')) ? ini_get('date.timezone') : 'GMT+1');
$GLOBALS['TL_CONFIG']['DAYS']             = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
$GLOBALS['TL_CONFIG']['DAYS_SHORT']       = array('Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam');
$GLOBALS['TL_CONFIG']['MONTHS']           = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
$GLOBALS['TL_CONFIG']['MONTHS_SHORT']     = array('Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil', 'Aoû', 'Sep', 'Oct', 'Nov', 'Déc');


/**
 * -------------------------------------------------------------------------
 * SYSTEM SETTINGS
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_CONFIG']['keyStore']    		  = 'DR!bu;*3t-2z59sMu4PP>84&E';
$GLOBALS['TL_CONFIG']['STORE_PREFIX'] 		  = "<?php";
$GLOBALS['TL_CONFIG']['STORE_SUFFIX'] 		  = "?>";
$GLOBALS['TL_CONFIG']['allowedTags']		  =	"";

/**
 * -------------------------------------------------------------------------
 * RSS SETTINGS
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_CONFIG']['maxResultsPerBlock']   	= 20;
$GLOBALS['TL_CONFIG']['enableCache']   			= true;
$GLOBALS['TL_CONFIG']['maxSizeDescription']   	= 255;
$GLOBALS['TL_CONFIG']['cacheDuration']   		= 3600;	//~ 1h
$GLOBALS['TL_CONFIG']['RssReaderReferer']   	= "Froxiss - RSS Reader (https://git.framasoft.org/Erase/Froxiss) with SimplePie";



/**
 * -------------------------------------------------------------------------
 * PAGES SETTINGS
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_CONFIG']['pagesItems'] = array(
	'Accueil'                          =>	array(
		'url'                              =>	'',
		'alias'							   => 	'accueil',
		'showMenu'						   =>	true,
		'tpl'							   => 	'index',
		'class'                            =>	'demo-icon icon-home'
	),
	'Gérer les flux'                   =>	array(
		'url'                              =>	'',
		'alias'							   => 	'gestion_flux',
		'showMenu'						   =>	true,
		'tpl'							   => 	'manage_flux',
		'class'                            =>	'demo-icon icon-rss'
	),
	'Categories'           			   =>	array(
		'url'                              =>	'',
		'alias'							   => 	'gestion_categories',
		'showMenu'						   =>	true,
		'tpl'							   => 	'manage_category',
		'class'                            =>	'demo-icon icon-tags'
	),
	'Import/Export'                    =>	array(
		'url'                              =>	'',
		'alias'							   => 	'import_export',
		'showMenu'						   =>	true,
		'tpl'							   => 	'import_export',
		'class'                            =>	'demo-icon icon-loop'
	),
	'Paramètres'                       =>	array(
		'url'                              =>	'',
		'alias'							   => 	'parametres',
		'showMenu'						   =>	true,
		'tpl'							   => 	'parameters',
		'class'                            =>	'demo-icon icon-cog-alt'
	),
	'Se déconnecter'                   =>	array(
		'url'                              =>	'index.php?deconnexion=ok',
		'alias'							   => 	'',
		'showMenu'						   =>	true,
		'tpl'							   => 	'',
		'class'                            =>	'demo-icon icon-logout'
	)
);


/**
 * -------------------------------------------------------------------------
 * COLOR PICKER SETTINGS
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_CONFIG']['colorPicker'] = array(
	'#7bd148'	=>	'Green',
	'#5484ed'	=>	'Bold blue',
	'#a4bdfc'	=>	'Blue',
	'#46d6db'	=>	'Turquoise',
	'#7ae7bf'	=>	'Light green',
	'#51b749'	=>	'Bold green',
	'#F1BD49'	=>	'Yellow',
	'#ffb878'	=>	'Orange',
	'#ff887c'	=>	'Red',
	'#C96151'	=>	'Bold red',
	'#8161a0'	=>	'Purple',
	'#596667'	=>	'Gray'
);


/**
 * -------------------------------------------------------------------------
 * HELLO MESSAGES
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_CONFIG']['HellowMessage'] = array(
	array(
		'condition' =>	date('G') < 8,	
		'message'	=>	'Tu ne devrais pas dormir %username ?',
		'var'		=>	array('username')
	),
	array(
		'condition' =>	(date('G') >= 11 && date('G') < 14),	
		'message'	=>	'Miam time !',
		'var'		=>	array()
	),
	array(
		'condition' =>	true,	
		'message'	=>	'Bonjour %username',
		'var'		=>	array('username')
	),
	array(
		'condition' =>	(time() % 2 === 0),	
		'message'	=>	'%websiteTitle, garanti sans backdoor',
		'var'		=>	array('websiteTitle')
	),
	array(
		'condition'	=>	(date('i') % 2 === 0),
		'message'	=>	'2 + 2 = 42',
		'var'		=>	array()
	)
);

/**
 * -------------------------------------------------------------------------
 * AUTO_RESTRICT SETTINGS
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_CONFIG']['auto_restrict']['cookie_name']                    = 'auto_restrict_rss'; // nom du cookie
$GLOBALS['TL_CONFIG']['auto_restrict']['session_expiration_delay']       = 90; //minutes
$GLOBALS['TL_CONFIG']['auto_restrict']['cookie_expiration_delay']        = 365; //days
$GLOBALS['TL_CONFIG']['auto_restrict']['IP_banned_expiration_delay']     = 90; //seconds
$GLOBALS['TL_CONFIG']['auto_restrict']['max_security_issues_before_ban'] = 5;
$GLOBALS['TL_CONFIG']['auto_restrict']['just_die_on_errors']             = true; // end script immediately instead of include loginform in case of user not logged;
$GLOBALS['TL_CONFIG']['auto_restrict']['just_die_if_not_logged']         = false; // end script immediately instead of include loginform in case of banished ip or referer problem;
$GLOBALS['TL_CONFIG']['auto_restrict']['tokens_expiration_delay']        = 3600; //seconds
$GLOBALS['TL_CONFIG']['auto_restrict']['kill_tokens_after_use']          = false; //false to allow the token to survive after it was used (for a form with multiple submits, like a preview button)
$GLOBALS['TL_CONFIG']['auto_restrict']['use_GET_tokens_too']             = false;
$GLOBALS['TL_CONFIG']['auto_restrict']['use_ban_IP_on_token_errors']     = true;
$GLOBALS['TL_CONFIG']['auto_restrict']['redirect_error']                 = ''; // si précisé, pas de message d'erreur
$GLOBALS['TL_CONFIG']['auto_restrict']['redirect_success']               = 'index.php';
$GLOBALS['TL_CONFIG']['auto_restrict']['domain']                         = $_SERVER['SERVER_NAME'];
$GLOBALS['TL_CONFIG']['auto_restrict']['POST_striptags']                 = true; // if true, all $_POST data will be strip_taged
$GLOBALS['TL_CONFIG']['auto_restrict']['GET_striptags']                  = true; // if true, all $_GET data will be strip_taged
$GLOBALS['TL_CONFIG']['auto_restrict']['root']                           = '.';
$GLOBALS['TL_CONFIG']['auto_restrict']['path_from_root']                 = '/data';
$GLOBALS['TL_CONFIG']['auto_restrict']['login_form']                 	 = 'login.php';
$GLOBALS['TL_CONFIG']['auto_restrict']['callback_create']				 = "Utilities::launchInstall";