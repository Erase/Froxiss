<footer id="footer">
    <div class="inside">
    	<ul>
    		<li>
    			<a href="https://git.framasoft.org/Erase/Froxiss/" target="_blank" title="Dépôt du projet">
    				Froxiss - Minimalist RSS Reader
    			</a>
    			<img src="./assets/froxiss/logo.png" id="logo_bottom" alt="Froxiss" />
    		</li>
    		<li>
    			Version <?php echo VERSION;?>
    		</li>
    		<li>
    			<a href="https://git.framasoft.org/Erase/Froxiss/blob/master/README.md" target="_blank" title="Le reste à connaître">A savoir</a>
    		</li>
    	</ul>    	
    </div>
</footer>