<meta charset="<?php echo Config::get('characterSet');?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo Config::get('websiteTitle');?></title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<base href="<?php echo Environment::get('base');?>" />
<link rel="icon" type="image/png" href="./assets/froxiss/favicon.png" />
<?php include TL_ROOT.'/system/inc/styles.php'; ?>