<header id="header">
    <div class="inside">
    	<?php
    		echo Menu::generate(null, array('class'=>'block resp', 'id'=>'menu_principal'));
    	?>
    	<div class="logo_container">
    		<img src="./assets/froxiss/logo.png" id="logo" alt="Froxiss" />
    	</div>
    	<div class="clear"></div>
    </div>
</header>	