<?php
	Theme::generateScript();
?>
<script>
	//~ Color picker
	var _colors = <?php echo json_encode($GLOBALS['TL_CONFIG']['colorPicker']);?>;

	//~ Token auto_restrict
	var Token = "<?php newToken(true); ?>";

	//~ Base url
	var Base = "<?php echo Environment::get('base');?>";

	//~ Try to hide referer
	var hideReferer = <?php echo (Config::get('hideReferer') ? "true" : "false");?>
</script>