<?php

	//~ Store the microtime
	define('TL_START', microtime(true));


	//~ Define the root path 
	define('TL_ROOT', dirname(__DIR__));


	//~ Include the helpers & Configuration
	require TL_ROOT.'/system/helper/functions.php';
	require TL_ROOT.'/system/config/constants.php';


	//~ Error handling
	@ini_set('error_log', TL_ROOT.'/system/logs/error.log');


	//~ Include class Config
	require TL_ROOT.'/system/classes/Config.php';
	Config::preload();

	//~ Adjust the error handling
	ini_set('display_errors', (Config::get('displayErrors') ? 1 : 0));
	error_reporting((Config::get('displayErrors') || Config::get('logErrors')) ? 1 : 0);


	//~ Define the relative path to the installation
	if (file_exists(TL_ROOT . '/system/config/pathconfig.php')){
		define('TL_PATH', include TL_ROOT . '/system/config/pathconfig.php');
	}else{
		define('TL_PATH', null); // cannot be reliably determined
	}


	//~ Include some classes required for further processing
	require TL_ROOT.'/system/classes/Utilities.php';
	require TL_ROOT.'/system/classes/Environment.php';
	require TL_ROOT.'/system/classes/Database.php';
	require TL_ROOT.'/system/classes/Session.php';
	require TL_ROOT.'/system/classes/Input.php';
	require TL_ROOT.'/system/classes/String.php';
	require TL_ROOT.'/system/classes/Date.php';
	require TL_ROOT.'/system/classes/Feed.php';
	require TL_ROOT.'/system/classes/FeedItem.php';
	require TL_ROOT.'/system/classes/FeedReader.php';
	require TL_ROOT.'/system/classes/OPML.php';
	require TL_ROOT.'/system/classes/Menu.php';
	require TL_ROOT.'/system/classes/Favicon.php';
	require TL_ROOT.'/system/classes/Theme.php';
	require TL_ROOT.'/system/classes/Template.php';


	//~ Vendor
	require TL_ROOT.'/system/vendor/simplepie/SimplePie.php';
	require TL_ROOT.'/system/vendor/favicondownloader/FaviconDownloader.class.php';
	require TL_ROOT.'/system/vendor/lib_opml/lib_opml.php';


	//~ Agent info
	$objAgent = Environment::get('agent');


	//~ Set params for auto_restrict
	Config::set('auto_restrict', array_merge(
		Config::get('auto_restrict'),
		array(
			'login'	=>	(array_key_exists('login', $_POST) ? $_POST['login'] : Config::get('username'))
		)
	));

	//~ Check auth exept rss steam
	$isRSSRequest = false;
	if(Input::get('do') === 'rss' && !is_null(Input::get('u')) && Input::get('u') === String::simpleCrypt(Config::get('username'))){
		$isRSSRequest = true;
	}else{
		$auto_restrict = array();
		foreach(Config::get('auto_restrict') as $opt => $val){
			$auto_restrict[$opt]	= $val;
		}
		require TL_ROOT.'/system/vendor/_auto_restrict/auto_restrict.php';
	}


	//~ Start the session
    @session_set_cookie_params(0, (!is_null(TL_PATH) ? TL_PATH : '/')); 
	@session_start();


	//~ Get the Config instance
	$objConfig = Config::getInstance();

	//~ Set the website path 
	Config::set('websitePath', TL_PATH);


	//~ Set the timezone
	@ini_set('date.timezone', Config::get('timeZone'));
	@date_default_timezone_set(Config::get('timeZone'));


	//~ Define the datastore file
	Config::set('dataStorePath', TL_ROOT.'/data/');
	Config::set('DataStoreFile', Config::get('dataStorePath').String::simpleCrypt(Config::get('username')));


	//~ Set database reference
	$db = new Database();

	//~ Ajax request
	if(Environment::get('isAjaxRequest')){
		if(array_key_exists('action', $_REQUEST) && $_REQUEST['action'] !== ""){
			require TL_ROOT.'/system/classes/Ajax.php';
			$action = $_REQUEST['action'];
			if(array_key_exists('do', $_REQUEST)){
				unset($_REQUEST['do']);
			}
			unset($_REQUEST['action']);
			new Ajax($action, $_REQUEST);
		}
    }

    //~ Rss request
    if($isRSSRequest){
    	$_data = $db->get('data');
        $_links = array();
        foreach($_data as $categorie){
            $_links = array_merge($categorie['links'], $_links);
        }

        if(count($_links)){
            $feeds = new FeedReader($_links);

            $oFeedGenerate = new Feed('feed_full');
            $oFeedGenerate->title = Config::get('websiteTitle');
            $oFeedGenerate->description = "Flux ".Config::get('websiteTitle')." complet de ".Config::get('username');
            $oFeedGenerate->link = Environment::get('base');
            $oFeedGenerate->published = time();

            foreach($feeds->arrItems as $item){
                $oItem = new FeedItem();
                $oItem->title = $item->get_title();
                $oItem->description = $item->get_description();
                $oItem->link = $item->get_link();
                $oItem->published = $item->get_date('U');
                $oItem->guid = $item->get_id(); 
                $oItem->permalink = $item->get_permalink();

                $oFeedGenerate->addItem($oItem);
            }
            ob_end_clean(); 
            ob_clean(); 
            header('Content-Type: application/xml');
            header("Content-Type: application/rss+xml; charset=".Config::get('characterSet'));
            flush();

            echo $oFeedGenerate->generateRss();
 
            exit;
        }
    }



?>